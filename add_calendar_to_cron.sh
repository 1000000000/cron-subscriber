#!/bin/bash

# usage: ./add_calendar_to_cron.sh <RC calendar token> <alarm command...>

token="$1"
shift
alarm="$(echo "$@" | tr -d "\n" | sed -e "s/[&\\\\\\/]/\\\\&/g")" # clobber any newlines and escape / \ & in command

jobsig="automated RC calendar"

if [ ! -d /tmp/crontab.* ]; then
	calendar="$(curl -s "https://www.recurse.com/calendar/events.ics?token=$token")"
	if [ "$?" -eq 0 ]; then
		# This will only create jobs for events in the current year
		cronjobs="$(echo "$calendar" | grep "^DTSTART;" | grep -o "$(date +%Y)....T.......$" | sed -e "s/^....\\([0-9][0-9]\\)\\([0-9][0-9]\\)T\\([0-9][0-9]\\)\\([0-9][0-9]\\)...$/\\4 \\3 \\2 \\1 * $alarm/")"
		prevcrontab="$(crontab -l 2> /dev/null | sed -e "s/\\\\/\\\\&/g")"
		sigcount="$(echo "$prevcrontab" | grep -m 3 -c -x "\(# begin $jobsig\|# end $jobsig\)")"
		# " <- needed to fix my syntax highlighting
		prevlinesend="$(echo "$prevcrontab" | fgrep -m 1 -n -x "# begin $jobsig" | cut -f1 -d:)"
		postlinesbegin="$(echo "$prevcrontab" | fgrep -m 1 -n -x "# end $jobsig" | cut -f1 -d:)"
		if [ ! -d /tmp/crontab.* ]; then
			if [ -z "$prevlinesend" ] || [ -z "$postlinesbegin" ] || [ "$prevlinesend" -ge "$postlinesbegin" ] || [ "$sigcount" -gt 2 ] ; then
				echo -e "$(echo "$prevcrontab" | grep -vx "\(# begin $jobsig\|# end $jobsig\)")\n# begin $jobsig\n$cronjobs\n# end $jobsig" | crontab -
				# " <- needed to fix my syntax highlighting
			else
				echo -e "$(echo "$prevcrontab" | head -n "$prevlinesend")\n$cronjobs\n$(echo "$prevcrontab" | tail -n "+$postlinesbegin")" | crontab -
			fi
		fi
	fi
fi

