# Cron Subscriber

A simple bash script that will add RC calendar events to your crontab.
It is recommended that you have your calendar settings to only subscribe to events you are RSVP'ed to.

### Features

* Does not delete existing cronjobs
* Does not modify the cron file outside of the section it brackets off for its jobs
* Does not attempt to modify the cron file if it has been opened by `crontab -e`
* Allows for custom alarm command

### Caveats

* This script will obliterate any cronjobs defined between the line containing the `# begin automated RC calendar` comment and the line containing the `# end automated RC calendar` comment.
* This script is *incredibly* conservative about potentially obliterating other cronjobs.
  As such if there is any slight deviation from the formatting it expects (exactly one line containing the starting comment appearing before exactly one line containing the end comment), the script will opt to not delete any definitions from the cron file and instead append the jobs to the end, correcting the formatting while it is at it.
  If something else breaks the formatting this could result in duplication of the RC calendar alarms.
* I'm pretty sure I ironed out all the potential for special characters in the cronfile and alarm command to cause trouble, but I would not be shocked if I missed something.

### Usage

```bash
./add_calendar_to_cron.sh <RC calendar token> <alarm command...>
```

Your RC calendar token can be found at the end of the subscription URL in your [calendar settings](https://www.recurse.com/settings/calendar).

I personally recommend making a cronjob to run this script so you don't need to remember to have it update the event cronjobs.
An example definition might look like so:

```
*/10 * * * * /path/to/script/add_calendar_to_cron.sh deadbeef beep
```

This will update the calendar events every 10 minutes and beep when an event starts.

